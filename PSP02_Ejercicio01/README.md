## TAREA PSP 02 - Ejercicio 1

## Instalación

Clonar el repositorio:

```
git clone https://paperezsi@bitbucket.org/paperezsi/psp2018-tarea02.git
git checkout ejercicio01
```




##Pasos para probar la ejecución del ejercicio 1.

Para la resolución de la tarea, creo un nuevo proyecto Java, al que añado 4 clases para modelar el problema propuesto.

Las clases implementadas son:
Aplicacion: Clase que lanza la aplicación, contiene el método main en el que creo e inicializo los hilos.
![0.png](readme_src/img/0.png)

BufferCompartido: Clase encargada de gestionar la exclusión mutua de los hilos al recurso compartido, en este caso el arry buffer.
![1.png](readme_src/img/1.png)

HiloConsumidorThread: Clase para simular el hilo consumidor de los caracteres del array, en este caso la clase extiende de Thread para implementar el comportamiento de hilo.
![2.png](readme_src/img/2.png)

HiloProductorRunnable: Clase para simular el hilo productor que rellena el array, en este caso la clase implementa Runnable en vez de Thread. Está planteado de esta forma para poder ver las dos formas de implementar un hilo de java.
![3.png](readme_src/img/3.png)


Las dos clases anteriores implementan el método run(), donde se realiza la acción concreta de cada una. En ambas dos se añade un método sleep que retrasa la acción 1 segundo, para poder visualizar el proceso de una forma más perceptible. También es de destacar que la cantidad de caracteres a producir y a consumir es de 15, mientras que el buffer tiene una capacidad de 6.


Para probar la aplicación el principal paso fue mostrar mensajes de salida para identificar que es lo que está ocurriendo en cada paso, además de probar el correcto funcionamiento de las regiones critimcas, en este caso, los metodos producciendo y consumiendo de la clasde BufferCompartida.

Estos dos métodos son synchronized, lo que asegura el uso en exclusión mutua de la variable buffer.

![4.png](readme_src/img/4.png)


* **RESULTADO FINAL:** El la salida final podemos comprobar el resultado de la ejecución, vemos que se van producciendo y consumiendo caracteres de forma aleatoria, en función de la disponibilidad en el buffer compartido de los mismos.




## Meta

Pablo Pérez – ue57919@edu.xunta.es

Distribuido bajo la licencia CC BY. Ver [``LICENCIAS``](https://creativecommons.org/licenses/?lang=es_ES) para más información.

[https://bitbucket.org/paperezsi/](https://bitbucket.org/paperezsi/)