/**
 * PSP02 - Ejercicio01 - Hilos Productor-Consumidor
 * 
 * Clase productor en este caso extendida desde Thread para la creaci�n de los
 * hilos.
 * 
 * @author Pablo
 */

public class HiloConsumidorThread extends Thread {

	// ===================================================
	// DEFINICI�N DE VARIABLES
	// ===================================================
	private BufferCompartido buffer;
	private String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private int longitudBuffer;


	// ===================================================
	// Construtor
	// ===================================================
	public HiloConsumidorThread(BufferCompartido buffer) {
		this.buffer = buffer;
		this.longitudBuffer = buffer.getLongitudBuffer();
	}

	
	// ===================================================
	// M�TODOS Y FUNCIONES
	// ===================================================
	@Override
	public void run() {
		
        char letra;
        int cantidadCaracteresConsumir = 15;
        
        //Consume 15 caracteres
        for( int i=0; i < cantidadCaracteresConsumir; i++ ) {
        	
            //Inicio el consumo e imprimo la letra consumida
        	letra = buffer.consumiendo();
            System.out.println( "Recogiendo el caracter "+ letra );
            
            // Duermo el hilo 1 segundo para ver el proceso.
            try {
                sleep( 1000);
            } catch( InterruptedException e ) {
            	System.err.println("Error metodo run de la clase HiloConsumidorThread");
            }
        }
    }

}
