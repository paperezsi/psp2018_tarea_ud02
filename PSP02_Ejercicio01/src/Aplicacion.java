/**
 * PSP02 - Ejercicio01 - Hilos Productor-Consumidor
 * 
 * Ejercicio del tipo productor-consumidor que mediante un hilo productor
 * almacene datos (15 caracteres) en un b�fer compartido, de donde los debe
 * recoger un hilo consumidor (consume 15 caracteres). La capacidad del b�fer
 * ahora es de 6 caracteres, de manera que el consumidor podr� estar cogiendo
 * caracteres del b�fer siempre que �ste no est� vac�o. El productor s�lo podr�
 * poner caracteres en el b�fer, cuando est� vac�o o haya espacio.
 * 
 * @author Pablo
 *
 */
public class Aplicacion {

	// ===================================================
	// M�TODO MAIN
	// ===================================================
	public static void main(String[] args) {
		
		// Creo los hilos de 2 formas distintas
		// Productor con runnable y consumidor directamente con Thread
		BufferCompartido buffer = new BufferCompartido();
		Runnable productorRunnable = new HiloProductorRunnable(buffer);
		Thread hiloProductor = new Thread(productorRunnable);
		Thread hiloConsumidor = new HiloConsumidorThread(buffer);

		// Inicializar los hilos
		hiloProductor.start();
		hiloConsumidor.start();
	}

}
