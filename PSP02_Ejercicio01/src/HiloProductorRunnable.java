/**
 * PSP02 - Ejercicio01 - Hilos Productor-Consumidor
 * 
 * Clase productor en este caso implementada con Runnable para la creaci�n de
 * los hilos.
 * 
 * @author Pablo
 */
public class HiloProductorRunnable implements Runnable {

	// ===================================================
	// DEFINICI�N DE VARIABLES
	// ===================================================

	private BufferCompartido buffer;
	private String alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private int longitudBuffer;
	

	// ===================================================
	// Construtor
	// ===================================================
	public HiloProductorRunnable(BufferCompartido buffer) {
		this.buffer = buffer;
		this.longitudBuffer = buffer.getLongitudBuffer();
	}

	
	// ===================================================
	// M�TODOS Y FUNCIONES
	// ===================================================
	@Override
	public void run() {

		char letra;
		int cantidadCaracteresProducir = 15;
		
		//Produce 15 caracteres
		for (int i = 0; i < cantidadCaracteresProducir; i++) {
			
			//Letras aleatorias
			letra = alfabeto.charAt((int) (Math.random() * 52));
			buffer.producciendo(letra);
			
			// Imprime un registro con lo a�adido
			System.out.println("Depositando el car�cter " + letra  + " en el buffer.");

			// Espera un poco antes de a�adir m�s letras
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.err.println("Error metodo run de la clase HiloProductorRunnable");
			}
		}
	}

}
