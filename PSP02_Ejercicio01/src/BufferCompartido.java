/**
 * PSP02 - Ejercicio01 - Hilos Productor-Consumidor
 * 
 * Clase controlador que sincroniza los hilos 
 * realizando las funciones de monitor para controlar las
 * zonas de exclusi�n mutua, en este caso la variable buffer. 
 * 
 * @author Pablo
 */
public class BufferCompartido {

	// ===================================================
	// DEFINICI�N DE VARIABLES
	// ===================================================
	private char[] buffer = null;
	private int longitudBuffer = 6;
	private int posicionBuffer = 0;
	private boolean bufferLleno = false;
	private boolean bufferVacio = true;

	
	// ===================================================
	// Construtor
	// ===================================================
	public BufferCompartido() {
		// Creo el buffer
		this.buffer = new char[longitudBuffer];
	}


	// ===================================================
	// M�TODOS Y FUNCIONES
	// ===================================================
	
	
	// M�todo para producir los caracteres
	public synchronized void producciendo(char letra) {

		//Mientra el buffer est� lleno espera dado que no
		//tiene espacio para a�adir m�s letras
		while (bufferLleno) {
			try {
				//Cuando bufferLleno esta a false se sale del bucle y
				//se comienza a producir.
				wait();
			} catch (InterruptedException e) {
				System.err.println("Error en producirCaracteres()");
			}
		}

		//Si deja de estar vacio produce nuevos caracteres
		buffer[posicionBuffer] = letra;
		posicionBuffer++;
		
		// Si el buffer est� lleno
		if (posicionBuffer == buffer.length) {
			bufferLleno = true;
		}else {
			bufferVacio = false;
		}
		// Despierta a los hilos
		notify();	
	}

	
	// Funci�n para consumir los caracteres
	public synchronized char consumiendo() {

		// Si el buffer est� vacio dormimos al
		// hilo consumidor, ya que no tiene nada que consumir
		while (bufferVacio) {
			try {
				//Sale del bucle cuando el bufferVacio es false
				wait();
			} catch (InterruptedException e) {
				System.err.println("Error en consumirCaracteres()");
			}
		}
		//Consumimos una letra
		posicionBuffer--;
		bufferLleno = false;

        if( posicionBuffer == 0 )
        	bufferVacio = true;
        // El buffer no puede estar lleno, porque acabamos de consumir
        bufferLleno = false;
        notify();

		char letra = buffer[posicionBuffer];
		return letra;

	}
	
	
	//M�todo get longitudBuffer
	public int getLongitudBuffer () {
		return this.longitudBuffer;
	}




}
