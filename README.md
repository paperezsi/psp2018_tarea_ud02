## TAREA PSP 02 - Ejercicios

## Instalación

Clonar el repositorio:

```
git clone git@bitbucket.org:paperezsi/psp2018-tarea02.git

git checkout ejercicio01
git checkout ejercicio02
```


Cada ejercicio contiene en su directorio un README especifico.


## Meta

Pablo Pérez – ue57919@edu.xunta.es

Distribuido bajo la licencia CC BY. Ver [``LICENCIAS``](https://creativecommons.org/licenses/?lang=es_ES) para más información.

[https://bitbucket.org/paperezsi/](https://bitbucket.org/paperezsi/)