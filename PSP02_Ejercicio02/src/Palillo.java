import java.util.concurrent.Semaphore;

/**
 * PSP02 - Ejercicio02 - Hilos Cena de los filósofos
 * 
 * Clase para simular los palillos
 * 
 * @author Pablo
 *
 */
public class Palillo {
	
	// ===================================================
	// DEFINICIÓN DE VARIABLES
	// ===================================================
	private boolean usandoPalillo;
	Semaphore semaforo;
	
	// ===================================================
	// Construtor
	// ===================================================
	public Palillo () {
		usandoPalillo = false;
		semaforo = new Semaphore(1);
	}

	
	// ===================================================
	// MÉTODOS Y FUNCIONES
	// ===================================================
	
	public void cogerPalillo() {
		while (usandoPalillo==true) {
			try {
				semaforo.acquire();
				
			}catch(Exception e) {
				System.out.println("Error en getPalillo");
			}
			usandoPalillo = true;
		}
	}
	
	public void dejarPalillo() {
		usandoPalillo = false;
		semaforo.release();
		
	}
	
	public boolean estaUsandoPalillo() {
		return usandoPalillo;
	}

}
