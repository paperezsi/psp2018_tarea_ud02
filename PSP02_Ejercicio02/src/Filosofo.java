 /**
 * PSP02 - Ejercicio02 - Hilos Cena de los fil�sofos
 * 
 * Clase para simular a los fil�sofo
 * 
 * @author Pablo
 *
 */
public class Filosofo extends Thread {
	
	// ===================================================
	// DEFINICI�N DE VARIABLES
	// ===================================================
	public int idFilosofo;
	public Palillo palilloIzquierda;
	public Palillo palilloDerecha;
	
	// ===================================================
	// Construtor
	// ===================================================
	public Filosofo(int idFilosofo, Palillo izquierda, Palillo derecha) {
		
		this.idFilosofo = idFilosofo;
		palilloIzquierda = izquierda;
		palilloDerecha = derecha;
	}

	// ===================================================
	// M�TODOS Y FUNCIONES
	// ===================================================
	@Override
	public void run() {
		pensar();
		comer();
	}
	
	//M�todo para poner a pensar durante un segundo al filosofo
	public void pensar() {
		
		System.out.println("El filosofo "+ idFilosofo + " est� pensando.");
		try {
			//Se duerme el hilo 1 segundo
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			System.err.println("Error clase filosofo en pensar.");
		}
	}

	
	//M�todo para poner a comer durante un segundo al filosofo
	public void comer() {
		if(!palilloIzquierda.estaUsandoPalillo())
		{
			if(!palilloDerecha.estaUsandoPalillo())
			{
				cojerPalillo();
				System.out.println("El filosofo "+idFilosofo+ " est� pensando.");
				try {
					//Se duerme el hilo 1 segundo
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.err.println("Error clase filosofo en pensar.");
				}
				dejarPalillo();
			}
		}
	}
	
	
	//M�todo para cojer los palillos
    public void cojerPalillo()
    {
    	palilloIzquierda.cogerPalillo();
    	System.out.println("El filosofo "+idFilosofo+ ": coge el palillo izquierdo.");
    	
    	palilloDerecha.cogerPalillo();
    	System.out.println("El filosofo "+idFilosofo+ ": coge el palillo derecho.");				
    }
    
    //M�todo para dejar los palillos
    public void dejarPalillo()
    {
    	palilloIzquierda.dejarPalillo();
    	System.out.println("El filosofo "+idFilosofo+ ": deja el palillo izquierdo.");
    	
    	palilloDerecha.dejarPalillo();
    	System.out.println("El filosofo "+idFilosofo+ ": deja el palillo derecho.");
    }
	

}
