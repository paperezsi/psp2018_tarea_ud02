 /**
 * PSP02 - Ejercicio02 - Hilos Cena de los fil�sofos
 * 
 * Resolver el cl�sico problema denominado "La cena de los fil�sofos"
 * utilizando la clase Semaphore del paquete java.util.concurrent.
 * El problema es el siguiente: Cinco fil�sofos se sientan alrededor de
 * una mesa y pasan su vida comiendo y pensando. Cada fil�sofo tiene un
 * plato de arroz chino y un palillo a la izquierda de su plato. Cuando un
 * fil�sofo quiere comer arroz, coger� los dos palillos de cada lado del plato
 * y comer�. El problema es el siguiente: establecer un ritual (algoritmo)
 * que permita comer a los fil�sofos. El algoritmo debe satisfacer la exclusi�n
 * mutua (dos fil�sofos no pueden emplear el mismo palillo a la vez), adem�s de
 * evitar el interbloqueo y la inanici�n.
 * 
 * @author Pablo
 *
 */

public class Main_Cena  {

	// ===================================================
	// M�TODO MAIN
	// ===================================================
	public static void main(String [] args) {
		int cantidadFilosofos = 5;
		final Filosofo filosofos[] = new Filosofo[cantidadFilosofos];
		Palillo palillos[] = new Palillo[cantidadFilosofos];
		
		for (int i = 0; i<cantidadFilosofos; i++) {
			palillos[i] = new Palillo();
		}
		
		for (int i=0; i< cantidadFilosofos; i++) {
			Palillo palilloIzquierda = palillos[i];
			Palillo palilloDerecha=palillos[(i+1)% cantidadFilosofos];
			
			if (i== filosofos.length -1) {
				//El �ltimo fil�sofo coje el tenedor del primero
				filosofos[i] = new Filosofo (i, palilloDerecha, palilloIzquierda);
			}else {
				filosofos[i] = new Filosofo (i, palilloIzquierda, palilloDerecha);
			}
			
			Thread hilo = new Thread(filosofos[i], "Filosofo " + (i+1));
			hilo.start();
		}
	}
	


}
