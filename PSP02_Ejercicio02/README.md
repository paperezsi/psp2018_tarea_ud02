## TAREA PSP 02 - Ejercicio 2

## Instalación

Clonar el repositorio:

```
git clone https://paperezsi@bitbucket.org/paperezsi/psp2018-tarea02.git
git checkout ejercicio02
```


##Pasos para probar la ejecución del ejercicio 1.

Para la resolución de la tarea02, referente al problema de la cena de los filósofos, creo un nuevo proyecto Java, al que añado en este caso 3 clases para modelar el problema.

Las clases implementadas son:

Main_Cena: Clase que lanza la aplicación, contiene el método main en el que creo e inicializo los hilos.

![1.png](readme_src/img/1.png)

Filosofo: Clase para representar a los filósofos y los palillos que este posee en cada momento, en esta clase están implementados los métodos de pensar y comer que duermen el hilo 1 segundo en cada caso, además implementa los métodos de coger y dejar los palillos.

![2.png](readme_src/img/2.png)

Palillo: Clase para simular los palillos en donde a través de un semáforo se gestiona la sección crítica de los palillos, para que cuando un filosofo los coje este recurso quede bloqueado a otros filósofos.

![3.png](readme_src/img/3.png)


Con el uso del semáforo mientras el palillo se está usando se bloquea ese recurso y cuando no se usa el palillo se desbloquea, para ello se hace uso de los métodos acquire() y release() de la clase Semaphore.

Para probar la aplicación lanzo la ejecución de la aplicación y confirmo que no se están solapando palillos que en principio están en uso por otros filósofos.

![4.png](readme_src/img/4.png)


* **RESULTADO FINAL:** El la salida final podemos comprobar el resultado de la ejecución, vemos que se los filósofos comienzan pensando y después por orden van cogiendo palillos hasta completar los palillos disponibles, el que se queda sin palillos continua pensando.



## Meta

Pablo Pérez – ue57919@edu.xunta.es

Distribuido bajo la licencia CC BY. Ver [``LICENCIAS``](https://creativecommons.org/licenses/?lang=es_ES) para más información.

[https://bitbucket.org/paperezsi/](https://bitbucket.org/paperezsi/)